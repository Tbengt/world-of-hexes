# World of Hexes
World of Hexes is a massively multiplayer online game. It is also massively multi-programmed, being open to anyone to contribute to.

## The world
The world consists of hexagonal pieces spanning the in-game equivalent of 500m from a vertex to the opposite vertex. Anyone is welcome to design a vertex and insert into the game world. However, when inserting a hexagon, the edge features must match with those of adjacent hexagons.

## The people of the world
Players are represented by avatars that can interact with other avatars.

## Classes
There are no classes. Characters are instead defined by what they do.
